import anime from "animejs";

import { start } from "./logo/logo";

document.onreadystatechange = function() {
  if (document.readyState === "complete") {
    anime({
      targets: ".network-icon",
      translateX: function(el) {
        return el.getAttribute("data-tx");
      },
      translateY: function(el, i) {
        return el.getAttribute("data-ty");
      },
      scale: function(el, i, l) {
        return el.getAttribute("data-s");
      },
      rotate: function(el) {
        return el.getAttribute("data-rot");
      },
      duration: function() {
        return anime.random(1200, 1800);
      },
      delay: function(el, i) {
        return 2500 + i * 500 + anime.random(0, 400);
      }
    });

    anime({
      targets: ".nav-home-item",
      translateX: function(el, i) {
        return (2 - i) * 500;
      },
      delay: function(el, i) {
        return 1500 + 300 * i;
      },
      duration: 500,
      easing: "easeOutCirc"
    });

    anime({
      targets: ".nav-sub-item",
      translateY: -200,
      delay: function(el, i) {
        return 1000 + anime.random(0, 400) + 100 * i;
      }
    });

    anime({
      targets: ".logo",
      opacity: 1,
      delay: 500,
      duration: 15000
    });

    setTimeout(start, 2000);
  }
};
