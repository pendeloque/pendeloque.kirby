# Pendeloque on Kirby CMS

---

### Requirements
Kirby runs on PHP 7.1+, Apache or Nginx

## Installation
Kirby does not require a database, which makes it very easy to
install. Just copy Kirby's files to your server and visit the
URL for your website in the browser.

**Please ensure that the `.htaccess` file has been
copied to the server correctly**
