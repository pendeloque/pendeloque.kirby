<section class="section textblock">
  <div class="content">
    <h4 class="subheading rellax"
        data-rellax-speed="-0.25"
        data-rellax-percentage="1.0">
      <?= $data->subheading() ?>
    </h4>
    <h3 class="heading rellax"
        data-rellax-speed="0.75"
        data-rellax-percentage="0.5">
      <?= $data->heading() ?>
    </h3>
    <article class="text rellax"
             data-rellax-speed="1.6"
             data-rellax-percentage="0.5">
      <?= $data->text()->kirbytext() ?>
    </article>
  </div>
</section>
