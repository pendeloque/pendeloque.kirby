<?php

return [
  'url' => '/',
  'debug' => true,
  'email' => [
    'transport' => [
      'type' => '',
      'host' => '',
      'port' => 80,
      'security' => true
    ]
  ],
  'panel' => [
    'install' => true
  ]
];
