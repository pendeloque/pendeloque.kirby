<?php snippet('header') ?>

<main>
  <header class="intro">
    <?php snippet('title') ?>
  </header>
  <div class="text narrow center">
    <?= $page->text() ?>
  </div>
  <br/>

  <form class="narrow" method="post">
    <div class="row">
      <div class="col">
        <label for="concern">Anliegen</label>
        <select id="concern" name="concern">
          <option value="collaboration">Kollaboration/Zusammenarbeit</option>
          <option value="project">Projektanfrage</option>
          <option value="misc">Sonstiges</option>
        </select>

        <label for="name">Name</label>
        <input name="name" id="name" type="text" required/>
        <small>
          <?php if(isset($alert) && isset($alert['name'])) { ?>
            <?= $alert["name"] ?>
          <?php } ?>
        </small>

        <label for="email">E-Mail</label>
        <input name="email" id="email" type="mail" required/>
        <small>
          <?php if(isset($alert) && isset($alert["email"])) { ?>
            <?= $alert["email"] ?>
          <?php } ?>
        </small>
      </div>

      <div class="col stretch">
        <label for="message">Nachricht</label>
        <textarea id="message" name="message" rows="15"></textarea>
        <small>
          <?php if(isset($alert) && isset($alert["message"])) { ?>
            <?= $alert["message"] ?>
          <?php } ?>
        </small>
      </div>
    </div>

    <div class="row center">
      <button type="submit" name="submit" value="Submit">Send</button>
    </div>
  </form>
  
</main>

<?php snippet('matomo') ?>
