<section>
  <h3><?= $data->heading() ?></h3>
  <p>
    <?= $data->text()->kirbytext() ?>
  </p>
  <iframe width="100%"
          frameborder="0"
          src="<?= $data->framesrc() ?>">
  </iframe>
</section>
