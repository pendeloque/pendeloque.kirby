<?php
 list($url, $videoID) = preg_split('/\?v=/', $data->video());
?>

<section class="section video">
  <div class="content">
    <div class="video">
      <iframe src="https://www.youtube.com/embed/<?= $videoID ?>?theme=dark&color=white&controls=0&showinfo=0&rel=0"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media;
                     gyroscope; picture-in-picture"
              allowfullscreen
              modestbranding="1">
      </iframe>
    </div>
  </div>
</section>

