<button class="sharing">
  <input type="checkbox" id="share-toggle" value="true"/>
  <ul class="share-wrapper">
    <!-- Twitter -->
    <a href="https://twitter.com/intent/tweet?url=<?= urlencode($url) ?>"
       class="share-icon twitter twitter-share-button"
       target="_blank">
      <?= snippet('icons/twitter') ?>
    </a>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    <!-- Facebook -->
    <div id="fb-root"></div>
    <script async defer
            crossorigin="anonymous"
            src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2"></script>
    <div class="share-icon facebook fb-share-button"
         data-href="https://developers.facebook.com/docs/plugins/"
         data-layout="button_count"
         data-size="large">
      <a target="_blank"
         href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode($url)?>%2F&amp;src=sdkpreparse"
         class="fb-xfbml-parse-ignore">
        <?= snippet('icons/facebook') ?>
      </a>
    </div>
  </ul>
  <label class="button-share" for="share-toggle">
  <svg width="35px" height="35px" viewBox="0 0 35 35">
    <g id="share" stroke="#FFFFFF" stroke-width="1" fill="#FFFFFF" fill-rule="evenodd">
      <circle cx="18" cy="24.5" r="4.5"></circle>
      <circle cx="7" cy="15.5" r="3"></circle>
      <circle cx="17.5" cy="8" r="3"></circle>
      <circle cx="28" cy="13.5" r="2.5"></circle>
      <path d="M7,15.5 L18,24.5"></path>
      <path d="M17.5,8 L18,24.5"></path>
      <path d="M28,13.5 L18,24.5"></path>
    </g>
  </svg>
  </label>
</button>

