<?php
$other_projects = pages(['dossier'])->children();
?>

<section class="next-projects">
  <h4 class="headline">Letzte Projekte</h4>
  <div class="content">
    <?php   $count = 1;
            foreach($other_projects->slice(0, 3) as $project): ?>
    <a href="<?= $project->url() ?>"
       class="next-project rellax"
       data-rellax-speed="<?= - cos($count) * 1.5 ?>"
       data-rellax-percentage="1.0"
       style="background-image: url('<?= $project->cover()->toFile()->url() ?>'">
      <div class="title-location">
        <h3 class="name"><?= $project->title() ?></h3>
        <div class="where">
          <h4 class="city"><?= $project->city() ?></h4>
          <h4 class="location"><?= $project->location() ?></h4>
        </div>
      </div>
      <div class="details">
        <div class="date">
          <span class="year"><?= $project->start()->toDate('Y') ?></span>
          <div class="days">
            <?= $project->start()->toDate('d.m.') ?>
            <?php if ($project->end() and
              $project->start()->toDate() != $project->end()->toDate()) { ?>
              —
              <span class="to"><?= $project->end()->toDate('d.m.') ?></span>
            <?php }; ?>
          </div>
        </div>
      </div>
    </a>
    <?php $count++; ?>
    <?php endforeach ?>
  </div>
</section>
