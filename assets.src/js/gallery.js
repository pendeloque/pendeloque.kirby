import Vue from "./vue.min.js";

const gallery = new Vue({
  el: "#gallery",
  data: {
    index: 0,
    stageClasses: [],
    classes: [],
    scaleProperty: "",
    images: Array.from(document.querySelectorAll("#images>img")).map(
      img => img.srcset
    ),
    // images: window.images, // -> Used for development
    fullscreen: false,
    autoSlideshow: false
  },

  mounted: function() {
    window.addEventListener("keyup", event => {
      switch (event.key) {
        case "ArrowRight":
          this.next();
          break;
        case "ArrowLeft":
          this.previous();
          break;
      }
    });
    this.startSlideshow();
  },
  // Computed properties are reactive bindings to
  // data blocks. They feature caching to improve performance
  // for computational heavy data transformations
  // (eg. looping through a large array). If you do not need
  // caching -> use a method!
  computed: {
    isOdd: function() {
      return this.index % 2;
    },
    presented: function() {
      return this.images[this.index % this.imageCount];
    },
    imageCount: function() {
      return this.images.length;
    },
    fadeTime: function() {
      const root = document.querySelector(":root");
      const styles = getComputedStyle(root);
      const fadeTime = styles.getPropertyValue("--fadeTimeInSec");
      const numerical = parseFloat(fadeTime) * 1000;
      return numerical;
    }
  },
  // Basically the same as computed properties, but without caching
  methods: {
    startSlideshow: function() {
      this.autoSlideshow = setInterval(() => {
        this.next();
      }, 10000);
    },
    previous: function() {
      this.classes = ["fade-out"];
      setTimeout(() => {
        this.index = this.index == 0 ? this.imageCount : this.index - 1;
        this.classes = [];
      }, this.fadeTime);
    },
    next: function() {
      this.classes = ["fade-out"];
      setTimeout(() => {
        this.index = (this.index + 1) % this.imageCount;
        this.classes = [];
      }, this.fadeTime);
    },
    userNext: function() {
      if (this.autoSlideshow) {
        clearInterval(this.autoSlideshow);
        setTimeout(() => this.startSlideshow(), 20000);
      }
      this.next();
    },
    toggleFullscreen: function() {
      this.fullscreen = !this.fullscreen;
    },

    select: function(index) {
      this.index = index;
    },

    onLoad: function(i) {
      const width = i.target.naturalWidth;
      const height = i.target.naturalHeight;
      if (width >= height) {
        this.scaleProperty = "width: 100%; height: auto;";
      } else {
        this.scaleProperty = "height: 100%; width: auto;";
      }
    }
  }
});
