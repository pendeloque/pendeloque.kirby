import anime from "animejs";
import Rellax from "rellax";

document.onreadystatechange = function() {
  if (document.readyState === "complete") {
    var x = window.matchMedia("(min-width: 768px)");
    if (x.matches) {
      var rellax = new Rellax(".rellax", {
        breakpoints: [480, 768, 1350],
        center: true
      });
    }
  }
};
