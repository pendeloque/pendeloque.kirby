<section>
  <h3><?= $data->title() ?></h3>
  <ul>
  <?php foreach($data->entries()->toStructure() as $list): ?>
    <li><?= $list->value() ?></li>
  <?php endforeach ?>
  </ul>
</section>
