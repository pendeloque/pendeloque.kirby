<?php snippet('header') ?>

<main>
  <header class="intro">
    <?php snippet('title') ?>
  </header>
  <div class="text">
    <?= $page->text()->kt() ?>
  </div>
</main>

<?php snippet('footer') ?>
