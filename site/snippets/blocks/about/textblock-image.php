<section class="info-section">
  <h4 class="preheading rellax"
      data-rellax-speed="0.5"
      data-rellax-percentage="0.5">
    <?= $data->preheading() ?>
  </h4>
  <h3 class="heading rellax"
      data-rellax-speed="2"
      data-rellax-percentage="0.5">
    <?= $data->heading() ?>
  </h3>
  <article class="textblock">
    <?= $data->article()->kirbytext() ?>
  </article>
  <?php if($data->image()->toFile() != null) { ?>
    <img class="image" srcset="<?= $data->image()->srcset([480, 800, 1280]) ?>">
  <?php } ?>
</section>
