<svg width="38px" height="40px" viewBox="0 0 38 40">
    <g id="Icon/Menu" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect id="Rectangle" fill="currentColor" x="0" y="6" width="25" height="6"></rect>
        <rect id="Rectangle-Copy" fill="currentColor" x="0" y="17" width="30" height="6"></rect>
        <rect id="Rectangle-Copy-2" fill="currentColor" x="0" y="28" width="17" height="6"></rect>
    </g>
</svg>
