<section class="info-section links">
  <h4 class="preheading rellax"
      data-rellax-speed="0.5"
      data-rellax-percentage="0.5">
    <?= $data->preheading() ?>
  </h4>
  <h3 class="heading rellax"
      data-rellax-speed="1.5"
      data-rellax-percentage="0.5">
    <?= $data->heading() ?>
  </h3>
  <ul class="list" data-columns="<?= $data->columncount() ?>">
  <?php
     $items = $data->entries()->toStructure();
     foreach ($items as $item): ?>
    <?php if ($item->link() != "") { ?>
      <li class="list-entry">
        <a href="<?= $item->link() ?>"><?= $item->text() ?></a>
      </li>
    <?php } else { ?>
    <li><?= $item->text() ?></li>
  <?php
      }
      endforeach
   ?>
  </ul>
</section>
