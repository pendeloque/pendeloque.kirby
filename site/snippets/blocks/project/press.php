<?php
$items = $data->articles()->toStructure();
?>

<section class="section press">
  <div class="content">
    <h3 class="heading rellax"
        data-rellax-speed="-0.5"
        data-rellax-percentage="1.0">
      <?= $data->pressheading()->kt()->or("Presse") ?>
    </h3>
    <div class="articles">
      <?php  $presscount = 1;
             foreach ($items as $item): ?>
        <a class="article rellax"
           href="<?= $item->url() ?>"
           target="_blank"
           data-rellax-speed="<?= $presscount * 1.25 ?>"
           data-rellax-percentage="0.5">
          <span class="date">
            <?php snippet('icons/press-article') ?>
             <?= $item->date()->toDate('d.m.Y') ?>
          </span>
          <h3 class="newstitle"><?= $item->heading()->html() ?></h3>
          <h4 class="source"><?= $item->source()->html() ?></h4>
        </a>
      <?php $presscount++; ?>
      <?php endforeach ?>
    </div>
  </div>
</section>
