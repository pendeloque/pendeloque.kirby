
<?php snippet('header') ?>

<main class="narrow dataprotection">

  <header class="intro">
    <?php snippet('title') ?>
    <h2><?= $page->heading() ?></h2>
  </header>

  <section>
    <?= $page->text()->kt() ?>
  </section>

  <section>
    <h3>Verantwortlicher</h3>
    <p>c/o <?= $page->prename() ?> <?= $page->surname() ?></p>
    <p><?= $page->address() ?></p>
    <p><?= $page->postcode() ?> <?= $page->city() ?></p>
  </section>

<?php foreach($page->builder()->toBuilderBlocks() as $block): ?>
  <?php snippet('blocks/dataprotection/' . $block->_key(),
                array('data' => $block)) ?>
<?php endforeach ?>

</main>

<?php snippet('footer', ['class' => "light"]) ?>
