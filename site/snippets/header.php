<?php
  if (!isset($navigation)) {
    $navigation = true;
  }
  function to_word($e) {
    return $e["word"];
  }
  $keywords = $page->keywords()->split();
?>

<!doctype html>
<html lang="de">
<head>
  <title><?= $site->title() ?> — <?= $page->title() ?></title>
  <meta charset="utf-8">
  <meta name="viewport"
        content="width=device-width,initial-scale=1.0">
  <meta property="description"
        content="<?= $site->title() ?> — <?= $page->title() ?>" />
  <meta property="keywords"
        content="<?= $page->keywords() ?>" />

  <!-- Facebook -->
  <meta property="og:site_name"
        content="<?= $site->title() ?>" />
  <meta property="og:title"
        content="<?= $page->title() ?>" />
  <meta property="og:type"
        content="article" />
  <?php if (null !== $page->seoimage()->toFile()) { ?>
  <meta property="og:image"
        content="<?= $page->seoimage()->toFile()->url() ?>"/>
  <?php } ?>
  <meta property="og:url"
        content="<?= $page->url() ?>"/>
  <meta property="og:description"
        content="<?= $page->description() ?>"/>

  <?= css(['assets/css/pendeloque.css', '@auto']) ?>
</head>
<body>
  <div class="page <?= isset($class) ? $class : '' ?>">
    <?php if ($navigation) snippet('navigation') ?>
