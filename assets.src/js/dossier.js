import anime from "animejs";
import Rellax from "rellax";

const triggerAt = 20;
let isAnimationRunning = false;
const hide = () => {
  var currentScrollPos = window.pageYOffset;
  if (isAnimationRunning) return;
  if (currentScrollPos > triggerAt) {
    anime({
      targets: ".dossier-header",
      translateY: "-60%",
      duration: 300,
      easing: "easeInOutCubic",
      begin: anim => (isAnimationRunning = true),
      complete: anim => (isAnimationRunning = false)
    });
    anime({
      targets: ".years",
      top: "0",
      duration: 300,
      easing: "easeInOutCubic"
    });
  } else {
    anime({
      targets: ".dossier-header",
      translateY: 0,
      duration: 300,
      easing: "easeInOutCubic",
      begin: anim => (isAnimationRunning = true),
      complete: anim => (isAnimationRunning = false)
    });
    anime({
      targets: ".years",
      top: "40px",
      duration: 300,
      easing: "easeInOutCubic"
    });
  }
};

document.onreadystatechange = function() {
  if (document.readyState === "complete") {
    var timeoutId = null;

    var x = window.matchMedia("(min-width: 768px)");
    if (x.matches) {
      document.addEventListener(
        "scroll",
        function() {
          if (timeoutId) clearTimeout(timeoutId);
          timeoutId = setTimeout(hide, 100);
        },
        true
      );
    }

    var x = window.matchMedia("(min-width: 768px)");
    if (x.matches) {
      var rellax = new Rellax(".rellax", {
        breakpoints: [480, 768, 1350],
        center: true
      });
    }
  }
};
