<section class="info-section">
  <h4 class="preheading rellax"
    data-rellax-speed="0.5"
    data-rellax-percentage="0.5">
    <?= $data->preheading() ?>
  </h4>
  <h3 class="heading rellax"
    data-rellax-speed="2"
    data-rellax-percentage="0.5">
    <?= $data->heading() ?>
  </h3>
  <article class="textblock">
    <?= $data->article()->kirbytext() ?>
  </article>
</section>
