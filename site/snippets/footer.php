<?php
$nav_sub = $site->children()->listed()->filter(function ($child) {
  return $child->menu() == 'sub';
});
?>
  </div> <!-- end of .page-wrapper -->

  <footer class="footer <?= $class ?>">
    <small class="copyright">&copy; 2015 — <?= date('Y') ?></small>
    <?php snippet('logo/pendeloque-small') ?>

    <div class="sub">
      <?php foreach ($nav_sub as $item): ?>
      <small class="nav-item"><?= $item->title()->link() ?></small>
      <?php endforeach ?>
    </div>
  </footer>

</body>
<?php snippet('matomo') ?>
</html>
