<?php
$nav_main = $site->children()->listed()->filter(function ($child) {
  return $child->menu() == 'main';
});
$nav_sub = $site->children()->listed()->filter(function ($child) {
  return $child->menu() == 'sub';
});
$backgrounds = $page->backgrounds()->toFiles();
$count = $backgrounds->count();
$index =  rand(0, $count - 1);
$background= $backgrounds->nth($index);
if ($background == NULL) {
  $background = "";
} else {
  $background = $background->url();
}
?>

<?php snippet('header', ['navigation' => false]) ?>

<main class="home">
  <div class="logo">
    <?= snippet('logo/pendeloque.classic') ?>
  </div>
  <nav class="nav-home">
    <?= snippet('icons/line') ?>
    <?php $navitem_count = 0;
          foreach ($nav_main as $item): ?>
    <p class="nav-home-item"
       data-x="<?= $navitem_count * 300 ?>">
      <?= $item->title()->link() ?>
    </p>
    <?php
        $navitem_count++;
        endforeach ?>
  </nav>
  <nav class="network">
    <a class="network-icon"
       href="<?= $page->facebook()->url() ?>"
       target="_blank"
       data-tx="150"
       data-ty="100"
       data-rot="20"
       data-s="1.5">
      <?= snippet("icons/network/facebook") ?>
    </a>
    <a class="network-icon"
       href="<?= $page->instagram()->url() ?>"
       target="_blank"
       data-tx="180"
       data-ty="-12"
       data-rot="-12"
       data-s="1.8">
      <?= snippet("icons/network/instagram") ?>
    </a>
    <a class="network-icon"
       href="<?= $page->twitter()->url() ?>"
       target="_blank"
       data-tx="130"
       data-ty="-120"
       data-rot="20"
       data-s="1.5">
      <?= snippet("icons/network/twitter") ?>
    </a>
  </nav>
  <nav class="nav-sub">
    <?php foreach ($nav_sub as $item): ?>
      <p class="nav-sub-item"><?= $item->title()->link() ?></p>
    <?php endforeach ?>
  </nav>
</main>

<?php snippet('matomo') ?>
<?= js('assets/js/home.js') ?>
