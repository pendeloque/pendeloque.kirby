<section class="section gallery">
  <div class="content">
    <div id="images">
      <?php foreach($data->images()->toFiles() as $img): ?>
        <img srcset="<?= $img->srcset([480, 800, 1280]) ?>">
      <?php endforeach ?>
    </div>

    <div id="gallery"
         class="gallery"
         v-bind:class="`${fullscreen ? 'fullscreen' : ''}`"
         v-on:keyup.right="userNext()"
         v-on:keyup.left="previous()">
      <div v-bind:class="['stage', `${fullscreen ? 'fullscreen' : ''}`].join(' ')">
        <button v-on:click="previous()"
                class="previous">
          <?php snippet('icons/gallery.prev') ?>
        </button>
        <img class="presented"
             v-bind:srcset="presented"
             v-on:load="onLoad"
             v-bind:class="classes.join(' ')"
             v-bind:style="scaleProperty"/>
        <img v-for="img in images"
             class="preload"
             v-bind:srcset="img"/>
        <button v-on:click="userNext()"
                class="next">
        <?php snippet('icons/gallery.next') ?>
        </button>
        <button v-on:click="toggleFullscreen()"
                class="toggle-fullscreen">
          <?php snippet('icons/fullscreen') ?>
        </button>
      </div>
    </div>
  </div>
</section>

<?= js('assets/js/gallery.js') ?>
