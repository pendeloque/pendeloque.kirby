<?php snippet('header') ?>

<main class="about-us">
<?php foreach($page->contentblocks()->toBuilderBlocks() as $block): ?>
  <?php snippet('blocks/about/' . $block->_key(),
                array('data' => $block)) ?>
<?php endforeach ?>
</main>

<?php snippet('footer', ['class' => "light"]) ?>
<?= js("assets/js/about.js") ?>
