<?php
function resolveConcern($concern) {
  switch ($concern) {
  case 'collaboration':
    return 'Kollaboration/Zusammenarbeit';
  case 'project':
    return 'Projectanfrage';
  case 'misc':
    return 'Sonstiges';
  }
}

return function($site, $pages, $page, $kirby) {
  $alert = null;
  if(get('submit')) {
    $data = array(
      'concern' => get('concern'),
      'name'    => get('name'),
      'email'   => get('email'),
      'message' => get('message')
    );
    $rules = array(
      'name'  => array('required'),
      'email' => array('required', 'email'),
      'message'  => array('required', 'min' => 3, 'max' => 3000),
    );
    $messages = array(
      'name'  => 'Bitte gib einen Namen an',
      'email' => 'Bitte verwende eine valide E-Mail Adresse',
      'message'  => 'Bitte schreib auch einen kleinen Text'
    );
    // some of the data is invalid
    if($invalid = invalid($data, $rules, $messages)) {
      $alert = $invalid;
      // the data is fine, let's send the email
    } else {
      // $body  = snippet('contactmail', $data, true);
      // build the email
        try {
          $concern = $data['concern'];
          $email = $kirby->email([
              'to'       => 'info@pendeloque.de',
              'from'     => 'noreply@pendeloque.de',
              // 'template' => 'contact',
              'replyTo'  => $data['email'],
              'subject' => 'testsubject',
              'body'    => 'testmail' 
              // 'data' => [
              //   'subject' => 'pendeloque.de — ' . resolveConcern($concern),
              //   'text'    => $data['message'],
              //   'email'   => $data['name']
              // ]
          ]);
          if($email->send()) {
            go('/kontakt');
          } else {
            $alert = array($email->error());
          }
        } catch (Exception $error) {
            echo $error;
        }
    }
    return compact('alert');
  }
};
