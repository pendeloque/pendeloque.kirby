<section class="section textimage">
  <div class="content">
    <h3 class="heading rellax"
        data-rellax-speed="1.25"
        data-rellax-percentage="1.0">
      <?= $data->heading() ?>
    </h3>
    <div class="text-col rellax"
         data-rellax-speed="-0.5"
         data-rellax-percentage="0.0">
      <article class="text"><?= $data->text()->kirbytext() ?></article>
    </div>
    <div class="image-col rellax"
         data-rellax-speed="-3.0"
         data-rellax-percentage="0.5">
      <img class="image"
           srcset="<?= $data->image()->toFile()->srcset([480, 800, 1280]) ?>"/>
    </div>
  </div>
</section>
