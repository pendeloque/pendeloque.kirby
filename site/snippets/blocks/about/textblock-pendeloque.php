<section class="info-section pdlq">
  <h4 class="preheading rellax"
      data-rellax-speed="0.5"
      data-rellax-percentage="0.5">
    <?= $data->preheading() ?>
  </h4>
  <h3 class="heading rellax"
      data-rellax-speed="1.25"
      data-rellax-percentage="0.5">
    <i><?= $data->heading() ?></i>
  </h3>
  <article class="textblock"><?= $data->description()->kirbytext() ?></article>

  <blockquote class="quoteblock">
    <span class="cite rellax"
          data-rellax-speed="-2"
          data-rellax-percentage="0.5">
      <span class="icon"><?php snippet('icons/cite') ?></span>
      <?= $data->cite()->kirbytext() ?>
    </span>
    <span class="cite-author rellax"
          data-rellax-speed="-2.25"
          data-rellax-percentage="0.5">
      <?= $data->author() ?>
    </span>
  </blockquote>

</section>
