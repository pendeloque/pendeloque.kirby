<?php snippet('header') ?>

<main class="wide imprint">
  <header>
    <?php snippet('title') ?>
  </header>

  <section class="row">
    <div class="contact col">
      <h3><?= $page->contactheading() ?></h3>
      <p><?= $page->contact() ?></p>
      <p><?= $page->street() ?></p>
      <p><?= $page->postcode() ?></p>
      <p><?= $page->city() ?></p>
      <p><?= $page->mail() ?></p>

      <div>
        <?php foreach($page->details()->toStructure() as $detail): ?>
          <a href="<?= $detail->link() ?>"
             target="_blank"
             class="row">
            <b class="col"><?= $detail->label() ?>:</b>
            <p><?= $detail->value() ?></p>
          </a>
        <?php endforeach; ?>
    </div>
  </section>

  <section class="legal col">
    <h3><?= $page->heading() ?></h3>
    <p><?= $page->legal() ?></p>
  </section>

</main>

<?php snippet('footer', ['class' => "light"]) ?>
