<svg class="line" width="39px" height="2px" viewBox="0 0 39 2" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Typography" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
        <g id="Landing-Page" transform="translate(-81.000000, -85.000000)" stroke="currentColor">
            <line x1="118.78487" y1="86" x2="82" y2="86" id="Path-11"></line>
        </g>
    </g>
</svg>
