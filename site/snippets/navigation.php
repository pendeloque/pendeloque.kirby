<?php
$nav_main = $site->children()->listed()->filter(function ($child) {
  return $child->menu() == 'main';
});
$nav_sub = $site->children()->listed()->filter(function ($child) {
  return $child->menu() == 'sub';
});
?>

<input type="checkbox" id="nav-toggle"/>
<label for="nav-toggle"
       class="nav-toggle-label">
  <?= snippet('icons/menu') ?>
</label>

<nav id="menu" class="menu">
  <input type="checkbox" id="nav-toggle" />
  <nav class="nav-wrapper">
    <div class="main">
      <?php foreach ($nav_main as $item): ?>
        <?= $item->title()->link() ?>
      <?php endforeach ?>
    </div>
    <div class="sub">
      <?php foreach ($nav_sub as $item): ?>
      <small class="nav-item"><?= $item->title()->link() ?></small>
      <?php endforeach ?>
    </div>
    <nav class="network">
      <a href="<?= $page->facebook()->url() ?>" target="_blank">
        <?= snippet("icons/network/facebook") ?>
      </a>
      <a href="<?= $page->instagram()->url() ?>" target="_blank">
        <?= snippet("icons/network/instagram") ?>
      </a>
      <a href="<?= $page->twitter()->url() ?>" target="_blank">
        <?= snippet("icons/network/twitter") ?>
      </a>
    </nav>
  </nav>
</nav>
