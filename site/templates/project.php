<?php snippet('header', ['class' => "project", 'navigation' => false]) ?>

<main class="project-main">
  <?php foreach($page->builder()->toBuilderBlocks() as $block): ?>
    <?php snippet('blocks/project/' . $block->_key(),
                  ['data' => $block,
                   'project' => $page]) ?>
  <?php endforeach ?>
</main>

<?php snippet('project/next-projects') ?>
<?= js('assets/js/project.js') ?>
<?php snippet('footer', ['class' => "dark project"]) ?>
