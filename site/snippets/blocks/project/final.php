
<section class="section final"
         style="background-image: url('<?= $data->background()->toFile()->url() ?>'">
  <div class="spacer first"></div>
  <div class="content"></div>
  <div class="spacer second"></div>
</section>
