<?php
switch ($rotation) {
case 'left':
  $angle = 0;
  break;
case 'right':
  $angle = 180;
  break;
}
?>
<svg class="icon-arrow" width="27px" height="10px" viewBox="0 0 27 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <g id="Icon/Arrow-Left" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <path d="M26.5,5 L18.5,10 L18.5,-1.83010477e-14 L26.5,5 Z M25.9979777,5 L1,5" id="Combined-Shape" stroke="<?= $color ?>" transform="translate(13.750000, 5.000000) rotate(<?= $angle ?>) scale(-1, 1) translate(-13.750000, -5.000000) "></path>
  </g>
</svg>
