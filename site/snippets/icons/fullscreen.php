<svg width="34px" height="34px" viewBox="0 0 34 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="Projects/Bond/Image-Gallery" transform="translate(-1197.000000, -49.000000)" stroke="#F8F3F1" stroke-width="2">
      <path d="M1206,50 L1230,50 L1230,74 L1206,74 L1206,50 Z M1198,66 L1214,66 L1214,82 L1198,82 L1198,66 Z" id="Combined-Shape"></path>
    </g>
  </g>
</svg>
