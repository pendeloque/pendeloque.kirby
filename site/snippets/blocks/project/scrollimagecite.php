<section class="section scrollimage-cite">
  <div class="content">
    <div class="cite rellax"
         data-rellax-speed="2.5"
         data-rellax-percentage="0.5">
      <span class="quotation-mark">
        <?php snippet('icons/cite') ?>
      </span>
      <cite class="citetext"><?= $data->quote() ?></cite>
    </div>
    <img class="image rellax"
         srcset="<?= $data->background()->toFile()->srcset([480, 800, 1280]) ?>"
         data-rellax-speed="-2"
         data-rellax-percentage="0.5"/>
  </div>
</section>
