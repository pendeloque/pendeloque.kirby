<svg width="50px" height="50px" viewBox="0 0 50 50" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Icons/Press-Article/A" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Group" transform="translate(8.000000, 12.000000)" fill-rule="nonzero" stroke="#FFFFFF" stroke-width="2.4">
            <line x1="-2.13162821e-14" y1="0.888182802" x2="26.126239" y2="0.888182802" id="Path-2"></line>
            <line x1="-7.63833441e-14" y1="9.0791916" x2="19.1281917" y2="9.0791916" id="Path-2-Copy"></line>
            <line x1="-1.13686838e-13" y1="17.2702004" x2="33.3371988" y2="17.2702004" id="Path-2-Copy-2"></line>
            <line x1="-8.17124146e-14" y1="25.0061532" x2="26.9202279" y2="25.0061532" id="Path-2-Copy-3"></line>
        </g>
    </g>
</svg>
