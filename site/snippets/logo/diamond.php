<svg width="142px" height="243px" viewBox="0 0 142 243">
  <defs>
    <linearGradient x1="180.952381%"
                    y1="-68.1732852%"
                    x2="-80.952381%"
                    y2="160.513207%"
                    id="linearGradient-1">
      <stop stop-color="#FFFFFF" offset="0%"></stop>
      <stop stop-color="#EEEEEF" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-261.904762%"
                    y1="335.398903%"
                    x2="261.904762%"
                    y2="-614.964353%"
                    id="linearGradient-2">
      <stop stop-color="#D9D9D9" offset="0%"></stop>
      <stop stop-color="#E8E8E9" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-900.459418%" y1="274.13705%" x2="447.166922%" y2="-219.285714%" id="linearGradient-3">
      <stop stop-color="#CBCBCB" offset="0%"></stop>
      <stop stop-color="#DFDFE0" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-297.297297%" y1="479.731102%" x2="297.297297%" y2="-203.75174%" id="linearGradient-4">
      <stop stop-color="#BFBFBF" offset="0%"></stop>
      <stop stop-color="#D8D8D9" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-197.297297%" y1="479.731102%" x2="397.297297%" y2="-203.75174%" id="linearGradient-5">
      <stop stop-color="#B8B8B8" offset="0%"></stop>
      <stop stop-color="#D4D4D5" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-347.166922%" y1="274.13705%" x2="1000.45942%" y2="-219.285714%" id="linearGradient-6">
      <stop stop-color="#C8C8C8" offset="0%"></stop>
      <stop stop-color="#DEDEDF" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-161.904762%" y1="335.398903%" x2="361.904762%" y2="-614.964353%" id="linearGradient-7">
      <stop stop-color="#DADADA" offset="0%"></stop>
      <stop stop-color="#E8E8E9" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-144.003527%" y1="200.870187%" x2="244.003527%" y2="-558.241758%" id="linearGradient-8">
      <stop stop-color="#A1A1A1" offset="0%"></stop>
      <stop stop-color="#ECECEC" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-273.149062%" y1="226.113676%" x2="161.204344%" y2="-283.038246%" id="linearGradient-9">
      <stop stop-color="#8F8F8F" offset="0%"></stop>
      <stop stop-color="#DDDDDE" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-230.627937%" y1="288.530162%" x2="145.279795%" y2="-135.993007%" id="linearGradient-10">
      <stop stop-color="#828282" offset="0%"></stop>
      <stop stop-color="#D1D1D3" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-170.22022%" y1="340.986301%" x2="270.22022%" y2="-13.5841447%" id="linearGradient-11">
      <stop stop-color="#757575" offset="0%"></stop>
      <stop stop-color="#C6C6C8" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-45.279795%" y1="288.530162%" x2="330.627937%" y2="-135.993007%" id="linearGradient-12">
      <stop stop-color="#909090" offset="0%"></stop>
      <stop stop-color="#DEDEDF" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-61.2043435%" y1="226.113676%" x2="373.149062%" y2="-283.038246%" id="linearGradient-13">
      <stop stop-color="#A5A5A5" offset="0%"></stop>
      <stop stop-color="#EFEFF0" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-139.240506%" y1="197.650926%" x2="139.240506%" y2="-537.234043%" id="linearGradient-14">
      <stop stop-color="#626262" offset="0%"></stop>
      <stop stop-color="#ECECEC" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-541.259321%" y1="183.690032%" x2="187.821044%" y2="-127.477477%" id="linearGradient-15">
      <stop stop-color="#585858" offset="0%"></stop>
      <stop stop-color="#DDDDDE" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-131.736527%" y1="258.961687%" x2="131.736527%" y2="-10.3164644%" id="linearGradient-16">
      <stop stop-color="#4F4F4F" offset="0%"></stop>
      <stop stop-color="#D1D1D3" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-31.7365269%" y1="258.961687%" x2="231.736527%" y2="-10.3164644%" id="linearGradient-17">
      <stop stop-color="#474747" offset="0%"></stop>
      <stop stop-color="#C6C6C8" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-87.8210439%" y1="183.690032%" x2="641.259321%" y2="-127.477477%" id="linearGradient-18">
      <stop stop-color="#585858" offset="0%"></stop>
      <stop stop-color="#DEDEDF" offset="100%"></stop>
    </linearGradient>
    <linearGradient x1="-39.2405063%" y1="197.650926%" x2="239.240506%" y2="-537.234043%" id="linearGradient-19">
      <stop stop-color="#656565" offset="0%"></stop>
      <stop stop-color="#EFEFF0" offset="100%"></stop>
    </linearGradient>
  </defs>
  <g id="Logo/Diamond" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g stroke-width="0.76349245">
      <polygon id="polygon200" stroke="url(#linearGradient-1)" fill="url(#linearGradient-1)" points="39.3562197 118.849558 35.1370288 177.918231 70.5782324 203.655295 106.019436 177.918231 101.800245 118.849558 70.5782324 76.206457"></polygon>
      <polygon id="polygon204" stroke="url(#linearGradient-2)" fill="url(#linearGradient-2)" points="94.5010448 208.586323 106.019436 177.918231 70.5782324 203.655295"></polygon>
      <polygon id="polygon206" stroke="url(#linearGradient-3)" fill="url(#linearGradient-3)" points="115.575903 151.34245 101.800245 118.849558 106.019436 177.918231"></polygon>
      <polygon id="polygon208" stroke="url(#linearGradient-4)" fill="url(#linearGradient-4)" points="91.653091 82.6870034 70.5782324 76.206457 101.800245 118.849558"></polygon>
      <polygon id="polygon210" stroke="url(#linearGradient-5)" fill="url(#linearGradient-5)" points="49.5033738 82.6870034 39.3562197 118.849558 70.5782324 76.206457"></polygon>
      <polygon id="polygon212" stroke="url(#linearGradient-6)" fill="url(#linearGradient-6)" points="25.5805614 151.34245 35.1370288 177.918231 39.3562197 118.849558"></polygon>
      <polygon id="polygon214" stroke="url(#linearGradient-7)" fill="url(#linearGradient-7)" points="46.65542 208.586323 70.5782324 203.655295 35.1370288 177.918231"></polygon>
      <polygon id="polygon218" stroke="url(#linearGradient-8)" fill="url(#linearGradient-8)" points="70.5782324 242.049932 94.5010448 208.586323 70.5782324 203.655295 46.65542 208.586323"></polygon>
      <polygon id="polygon220" stroke="url(#linearGradient-9)" fill="url(#linearGradient-9)" points="137.241449 202.389538 115.575903 151.34245 106.019436 177.918231 94.5010448 208.586323"></polygon>
      <polygon id="polygon222" stroke="url(#linearGradient-10)" fill="url(#linearGradient-10)" points="141.038721 108.7235 91.653091 82.6870034 101.800245 118.849558 115.575903 151.34245"></polygon>
      <polygon id="polygon224" stroke="url(#linearGradient-11)" fill="url(#linearGradient-11)" points="70.5782324 0.486616602 49.5033738 82.6870034 70.5782324 76.206457 91.653091 82.6870034"></polygon>
      <polygon id="polygon226" stroke="url(#linearGradient-12)" fill="url(#linearGradient-12)" points="0.117744302 108.7235 25.5805614 151.34245 39.3562197 118.849558 49.5033738 82.6870034"></polygon>
      <polygon id="polygon228" stroke="url(#linearGradient-13)" fill="url(#linearGradient-13)" points="3.91501612 202.389538 46.65542 208.586323 35.1370288 177.918231 25.5805614 151.34245"></polygon>
      <polygon id="polygon232" stroke="url(#linearGradient-14)" fill="url(#linearGradient-14)" points="70.5782324 242.049932 94.5010448 208.586323 137.241449 202.389538"></polygon>
      <polygon id="polygon234" stroke="url(#linearGradient-15)" fill="url(#linearGradient-15)" points="137.241449 202.389538 115.575903 151.34245 141.038721 108.7235"></polygon>
      <polygon id="polygon236" stroke="url(#linearGradient-16)" fill="url(#linearGradient-16)" points="91.653091 82.6870034 70.5782324 0.486616602 141.038721 108.7235"></polygon>
      <polygon id="polygon238" stroke="url(#linearGradient-17)" fill="url(#linearGradient-17)" points="70.5782324 0.486616602 49.5033738 82.6870034 0.117744302 108.7235"></polygon>
      <polygon id="polygon240" stroke="url(#linearGradient-18)" fill="url(#linearGradient-18)" points="0.117744302 108.7235 25.5805614 151.34245 3.91501612 202.389538"></polygon>
      <polygon id="polygon242" stroke="url(#linearGradient-19)" fill="url(#linearGradient-19)" points="3.91501612 202.389538 46.65542 208.586323 70.5782324 242.049932"></polygon>
    </g>
  </g>
</svg>
