<?php
$grouped_projects = $page->children()
                         ->sortBy('start', 'desc')
                         ->group(function($p) {
                             return $p->start()->toDate('Y');
                           });
?>
<?php snippet('header', ['class' => 'dossier']) ?>

<main class="dossier">
  <header class="dossier-header">
    <h4 class="preheading"><?= $page->preheading() ?></h4>
    <?php snippet('title') ?>
    <div class="years">
      <?php foreach($grouped_projects as $year => $items): ?>
        <a href="#<?= $year ?>" class="year">
          <span>#</span><?= $year ?>
        </a>
      <?php endforeach; ?>
    </div>
  </header>


  <section class="dossier-projects wide">
    <?php $count = 0; ?>
    <?php foreach($grouped_projects as $year => $projects): ?>
      <span class="year-section rellax section-<?= $count % 3 ?>"
            id="<?= $year ?>"
            data-rellax-speed="-2.5"
            data-rellax-mobile-speed="-1.5">
        <h3 class="year">
          <?= $year ?>
        </h3>
      </span>
      <div class="projects-per-year">
        <?php foreach($projects as $project): ?>
        <a href="<?= $project->url() ?>"
           class="dossier-project">
          <?php if ($project->cover()->toFile()) { ?>
          <div class="dates rellax"
               data-rellax-speed="1.5">
            <?= $project->start()->toDate('d.m.') ?>
            <?php if ($project->end() and
            $project->start()->toDate() != $project->end()->toDate()) { ?>
              <?= snippet('icons/arrow', ['color' => 'black', 'rotation' => 'right'])?>
            <?= $project->end()->toDate('d.m.') ?>
            <?php }; ?>
          </div>
          <div class="hero-image"
               style="background-image: url('<?= $project->cover()->toFile()->url() ?>')"
               data-rellax-speed="0.5">
            <?php } else { ?>
            <div class="hero-image"
                 style="background-color: var(--color-background-dark)">
              <?php }; ?>
            </div>
            <div class="hero-infos rellax"
                 data-rellax-speed="1.5">
              <h4 class="location">
                <?= $project->location() ?>,
                <?= $project->city() ?>
              </h4>
              <h2 class="name"><?= $project->title() ?></h2>
            </div>
        </a>
        <?php endforeach; ?>
      </div>
    <?php $count++; ?>
    <?php endforeach ?>
  </section>
</main>

<?php snippet('footer', ['class' => "light"]) ?>
<?= js('assets/js/dossier.js') ?>
