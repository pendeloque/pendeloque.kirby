<svg width="27px" height="24px" viewBox="0 0 27 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Projects/Bond/Image-Gallery" transform="translate(-1204.000000, -317.000000)" stroke="#F8F3F1" stroke-width="2">
            <g id="Group" transform="translate(50.000000, 319.000000)">
                <g id="Group-11" transform="translate(1167.000000, 10.000000) rotate(-360.000000) translate(-1167.000000, -10.000000) translate(1154.000000, 0.000000)">
                    <polygon id="Triangle" transform="translate(17.000000, 10.000000) rotate(-270.000000) translate(-17.000000, -10.000000) " points="17 2 27 18 7 18"></polygon>
                    <line x1="25.7828734" y1="10" x2="0" y2="10" id="Path-17"></line>
                </g>
            </g>
        </g>
    </g>
</svg>
