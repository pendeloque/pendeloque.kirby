<section class="section title"
         style="background-image: url('<?= $data->background()->toFile()->url() ?>'">
  <div class="content">
    <div class="topbar"
         data-rellax-speed="-5">
      <a class="back-button" href="/dossier">
        <?= snippet('icons/arrow', ['rotation' => 'left', 'color' => 'white']) ?>
      </a>
      <div class="location">
        <h4 class="subheadline"><?= $page->location() ?></h4>
        <h2 class="headline"><?= $page->city() ?></h2>
      </div>
    </div>
    <div class="dates"
         data-rellax-speed="-5">
      <span class="year"><?= $project->start()->toDate('Y') ?></span>
      <div class="day-dates">
        <span class="from"><?= $project->start()->toDate('d.m.') ?></span>
      <?php if ($project->end() and
                $project->start()->toDate() != $project->end()->toDate()) { ?>
        <span class="arrow">
          <?= snippet('icons/arrow', ['rotation' => 'right', 'color' => 'black']) ?>
        </span>
        <span class="to"><?= $project->end()->toDate('d.m.') ?></span>
      <?php }; ?>
      </div>
    </div>

    <div class="titles">
      <h3 class="subtitle rellax"
          data-rellax-speed="1.75">
        <?= $page->subtitle() ?>
      </h3>
      <h1 class="title rellax"
          data-rellax-speed="3">
        <?= $page->title() ?>
      </h1>
    </div>
  </div>
  <div class="spacer dark"></div>
</section>
